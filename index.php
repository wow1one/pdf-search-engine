<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/bootstrap.php';

use Services\EntityManagerService;
use Models\PdfFile;
use Models\PdfPage;

$entityManager = EntityManagerService::create();

if (!empty($_GET['parse'])) {
    $parser = new \Smalot\PdfParser\Parser();

    $path = __DIR__ . '/pdf';
    $files = array_diff(scandir($path), array('..', '.'));
    $numSynchronizedFiles = 0;
    $namesSynchronizedFiles = array();

    foreach ($files as $file) {
        $fullpath =  $path . "/" . $file;

        $pdfFile = $entityManager->getRepository('Models\PdfFile')->findOneByName($file);
        if($pdfFile) { continue; }

        $pdf = $parser->parseFile($fullpath);

        $pdfFile = new PdfFile();
        $pdfFile->setName($file);
        // $pdfFile->setPath($fullpath);
        // $pdfFile->setContent(preg_replace('/\s+/', ' ', $pdf->getText()));
        // $entityManager->persist($pdfFile);

        // Retrieve all pages from the pdf file.
        $pages  = $pdf->getPages();
        $pageNum = 0;

        // Loop over each page to extract text.
        foreach ($pages as $page) {

            $pageNum = $pageNum + 1;
            $pageText = preg_replace('/\s{2,}/', ' ', $page->getText());

            $pdfPage = new PdfPage();
            $pdfPage->setPdfFile($pdfFile);
            $pdfPage->setPageNum($pageNum);
            $pdfPage->setContent($pageText);
            $pdfFile->addPage($pdfPage);
        }
        
        $entityManager->persist($pdfFile);

        $numSynchronizedFiles ++;
        array_push($namesSynchronizedFiles, $file);
    }

    $entityManager->flush();

    if($numSynchronizedFiles > 0) {
        echo $numSynchronizedFiles . " files successfully synchronized!!!<br/><br/>";
        print_r($namesSynchronizedFiles);
    } else {
        echo "Files are already synchronized";
    }

} else {

    echo "
    <!DOCTYPE html>
    <html>
    <body>
    <form action='http://localhost/pdfparser/'>
    Search:<br>
    <input type='text' name='search' value='". (!empty($_GET['search']) ? $_GET['search'] : "hema") . "'>
    <br><br>
    <input type='submit' value='Submit'>
    </form> 
    <br><br>
    </body>
    </html>
    ";

    if (!empty($_GET['search'])) {
        // $searchResults = $entityManager->getRepository("Models\PdfFile")->createQueryBuilder('pdfFile')
        //     ->where('pdfFile.content LIKE :content')
        //     ->setParameter('content', '%'.$_GET['search'].'%')
        //     ->getQuery()
        //     ->getResult();

        // $query = $entityManager->createQuery("SELECT f FROM Models\PdfFile f JOIN f.pages p WHERE p.content LIKE '%".$_GET['search']."%'");
        $query = $entityManager->createQuery("SELECT f, p FROM Models\PdfFile f JOIN f.pages p WHERE f.id = p.pdfFile AND p.content LIKE '%".$_GET['search']."%'  
        ORDER BY p.pageNum ASC");
        $searchResults = $query->getResult();

        if(!empty($searchResults)) {
            foreach ($searchResults as $pdfFile) {
                echo "<a href=\"./pdf/" . $pdfFile->getName() . "\">" . $pdfFile->getName() . "</a><br/><br/>";
                foreach ($pdfFile->getPages() as $page) {
                    echo "&nbsp;&nbsp;&nbsp; - " . $page->getPageNum() ."<br/>";
                    $pageContent = $page->getContent();
                    $searchKeywords = preg_quote($_GET['search'], '/');
                    preg_match('/(.{0,50})('.$searchKeywords.')(.{0,50})/iu', $pageContent, $matches);
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ..." . $matches[1] . "<b style='background-color: rgba(255,255,0,0.5)'>" . $matches[2] . "</b>" . $matches[3] . "...<br/><br/>";
                }
            }
        } else {
            echo "Nothing found<br/>";
        }
        
    }

}

#############################################

// $pdf = $parser->parseFile('Quarterly Reports - Q3-2017.pdf');

// $pdfFile = new PdfFile();
// $pdfFile->setName("Quarterly Reports - Q3-2017");
// $pdfFile->setPath(__DIR__ . "/Quarterly Reports - Q3-2017.pdf");
// $pdfFile->setContent(preg_replace('/\s+/', ' ', $pdf->getText()));

// $entityManager->persist($pdfFile);
// $entityManager->flush();

// echo "Success!!!";

##############################################

// // Retrieve all details from the pdf file.
// $details  = $pdf->getDetails();
 
// // Loop over each property to extract values (string or array).
// foreach ($details as $property => $value) {
//     if (is_array($value)) {
//         $value = implode(', ', $value);
//     }
//     echo $property . ' => ' . $value . "\n";
// }

// // Retrieve all pages from the pdf file.
// $pages  = $pdf->getPages();
 
// // Loop over each page to extract text.
// foreach ($pages as $page) {

//     $pieces = explode("\n", $page->getText());

//     foreach ($pieces as $text) {
//         echo $text . "<br/>";
//     }

//     // echo $page->getText();
// }

// echo "<br><br><br><br><br>";