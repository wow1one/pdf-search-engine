<?php

use Services\EntityManagerService;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

//autoload
require_once(__DIR__ . '/vendor/autoload.php');

//load the entity manager
require_once(__DIR__ . '/src/Services/EntityManagerService.php');

//database config
require_once(__DIR__ . '/config/config.php');

return ConsoleRunner::createHelperSet(EntityManagerService::create());
//REVERSE DATABASE
//php ./vendor/doctrine/orm/bin/doctrine orm:convert-mapping --force --from-database annotation ./src/Models/entity/

//To Database
//php ./vendor/doctrine/orm/bin/doctrine orm:schema-tool:create

//To Database
//php ./vendor/doctrine/orm/bin/doctrine orm:schema-tool:update --force

//cache clear
// php ./vendor/doctrine/orm/bin/doctrine orm:clear-cache:metadata
// php ./vendor/doctrine/orm/bin/doctrine orm:clear-cache:query
// php ./vendor/doctrine/orm/bin/doctrine orm:clear-cache:result