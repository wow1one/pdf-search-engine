

var url = './pdf/Quarterly Reports - Q2-2017.pdf';

window.onload = function () {

    // PDFJS.getDocument(url)
    // .then(function(pdf) {

    //     // Get div#container and cache it for later use
    //     var container = document.getElementById("container");

    //     // Loop from 1 to total_number_of_pages in PDF document
    //     for (var i = 1; i <= pdf.numPages; i++) {

    //         // Get desired page
    //         pdf.getPage(i).then(function(page) {

    //         var scale = 1.5;
    //         var viewport = page.getViewport(scale);
    //         var div = document.createElement("div");

    //         // Set id attribute with page-#{pdf_page_number} format
    //         div.setAttribute("id", "page-" + (page.pageIndex + 1));

    //         // This will keep positions of child elements as per our needs
    //         div.setAttribute("style", "position: relative");

    //         // Append div within div#container
    //         container.appendChild(div);

    //         // Create a new Canvas element
    //         var canvas = document.createElement("canvas");

    //         // Append Canvas within div#page-#{pdf_page_number}
    //         div.appendChild(canvas);

    //         var context = canvas.getContext('2d');
    //         canvas.height = viewport.height;
    //         canvas.width = viewport.width;

    //         var renderContext = {
    //             canvasContext: context,
    //             viewport: viewport
    //         };

    //         // Render PDF page
    //         page.render(renderContext)
    //         .then(function() {
    //             // Get text-fragments
    //             return page.getTextContent();
    //         })
    //         .then(function(textContent) {
    //             // Create div which will hold text-fragments
    //             var textLayerDiv = document.createElement("div");

    //             // Set it's class to textLayer which have required CSS styles
    //             textLayerDiv.setAttribute("class", "textLayer");

    //             // Append newly created div in `div#page-#{pdf_page_number}`
    //             div.appendChild(textLayerDiv);

    //             // Create new instance of TextLayerBuilder class
    //             var textLayer = new TextLayerBuilder({
    //             textLayerDiv: textLayerDiv, 
    //             pageIndex: page.pageIndex,
    //             viewport: viewport
    //             });

    //             // Set text-fragments
    //             textLayer.setTextContent(textContent);

    //             // Render text-fragments
    //             textLayer.render();
    //         });
    //         });
    //     }
    // });
};

////////////////////////////////////////////////////////////////////////

// PDFJS.getDocument(url).then(function (pdfDocument) {
//   console.log('Number of pages: ' + pdfDocument.numPages);
// });

////////////////////////////////////////////////////////////////////////

window.onload = function () {

    var pdfDoc = null,
    pageNum = 1,
    pageRendering = false,
    pageNumPending = null,
    scale = 1.2,
    // canvas = document.getElementById('the-canvas'),
    // ctx = canvas.getContext('2d');
    container = document.getElementById("container");

    var pdfPageWidth = 0;
    var pdfPageHeight = 0;

    // function renderPage(pageNum, xOffset, yOffset) {
    //     pageRendering = true;
    //     // Using promise to fetch the page
    //     pdfDoc.getPage(pageNum).then(function(page) {

    //         page.getTextContent({normalizeWhitespace: true}).then(function(textContent) {
    //             let textItems = textContent.items;
    //             let strBuf = [];
      
    //             for (let j = 0, jj = textItems.length; j < jj; j++) {
    //               strBuf.push(textItems[j].str);
    //             }

    //             console.log(strBuf.join(''));
    //             // Store the pageContent as a string.
    //             // this.pageContents[i] = strBuf.join('');
    //             // extractTextCapability.resolve(i);
    //           }, function(reason) {
    //             console.error(`Unable to get page text content`, reason);
    //             // Page error -- assuming no text content.
    //             // this.pageContents[i] = '';
    //             // extractTextCapability.resolve(i);
    //           });

              
    //           var pdfViewBox = page.pageInfo.view;
    //           var viewport = new PDFJS.PageViewport(pdfViewBox, scale, page.rotate, -xOffset, -yOffset);
    //         // pdfPageWidth = pdfViewBox[2];
    //         // pdfPageHeight = pdfViewBox[3];
    //         // canvas.height = pdfViewBox[2];
    //         // canvas.width = pdfViewBox[3];
    //         pdfPageWidth = viewport.height;
    //         pdfPageHeight = viewport.width;
    //         canvas.height = viewport.height;
    //         canvas.width = viewport.width;
    
    
    //         // Render PDF page into canvas context
    //         var renderContext = {
    //             canvasContext : ctx,
    //             viewport : viewport
    //         };
    //         var renderTask = page.render(renderContext);
    
    //         // Wait for rendering to finish
    //         renderTask.promise.then(function() {
    //             pageRendering = false;
    //             if (pageNumPending !== null) {
    //                 // New page rendering is pending
    //                 renderPage(pageNumPending, pageLeftPending, pageTopPending);
    //                 pageNumPending = null;
    //                 pageLeftPending = 0;
    //                 pageTopPending = 0;
    //             }
    //         });
    
    //     });
    // }
    
    /**
     * Get page info from document, resize canvas accordingly, and render page.
     * @param num Page number.
     */
    function renderPage(num) {
      pageRendering = true;
      // Using promise to fetch the page
      pdfDoc.getPage(num).then(function(page) {

        // Create a new Canvas element
        var canvas = document.createElement("canvas");

        var viewport = page.getViewport(scale);

        // var viewport = page.getViewport(scale);
        pdfPageWidth = Math.ceil(viewport.width);
        pdfPageHeight = Math.ceil(viewport.height);
        canvas.height = viewport.height;
        canvas.width = viewport.width;

        container.setAttribute("style", "width: " + pdfPageWidth + "px; height: " + pdfPageHeight + "px;");
        
        var div = document.createElement("div");

        // Set id attribute with page-#{pdf_page_number} format
        // div.setAttribute("id", "page-" + (page.pageIndex + 1));
        div.setAttribute("id", "page");

        // This will keep positions of child elements as per our needs
        div.setAttribute("style", "position: relative; width: " + pdfPageWidth + "px; height: " + pdfPageHeight + "px;");

        // Append div within div#container
        // var currentPageDiv = document.getElementById("page-" + (page.pageIndex));
        // if(currentPageDiv) {container.removeChild(currentPageDiv);}
        // var currentPageDiv = document.getElementById("page-" + (page.pageIndex + 2));
        // if(currentPageDiv) {container.removeChild(currentPageDiv);}
        var currentPageDiv = document.getElementById("page");
        if(currentPageDiv) {container.removeChild(currentPageDiv);}
        container.appendChild(div);

        // Append Canvas within div#page-#{pdf_page_number}
        div.appendChild(canvas);

        var ctx = canvas.getContext('2d');

        // Render PDF page into canvas context
        var renderContext = {
          canvasContext: ctx,
          viewport: viewport
        };
        var renderTask = page.render(renderContext);

        // Wait for rendering to finish
        renderTask.promise.then(function() {
          pageRendering = false;
          if (pageNumPending !== null) {
            // New page rendering is pending
            renderPage(pageNumPending);
            pageNumPending = null;
          }

          return page.getTextContent();
        })
        .then(function(textContent) {
            // Create div which will hold text-fragments
            var textLayerDiv = document.createElement("div");

            // Set it's class to textLayer which have required CSS styles
            textLayerDiv.setAttribute("class", "textLayer");
            textLayerDiv.setAttribute("style", "width: " + pdfPageWidth + "px; height: " + pdfPageHeight + "px;");

            // Append newly created div in `div#page-#{pdf_page_number}`
            div.appendChild(textLayerDiv);

            // Create new instance of TextLayerBuilder class
            var textLayer = new TextLayerBuilder({
            textLayerDiv: textLayerDiv, 
            pageIndex: page.pageIndex,
            viewport: viewport
            });

            // Set text-fragments
            textLayer.setTextContent(textContent);

            // Render text-fragments
            textLayer.render();
        });
      });

      // Update page counters
      document.getElementById('page_num').textContent = num;
    }

    /**
     * If another page rendering in progress, waits until the rendering is
     * finised. Otherwise, executes rendering immediately.
     */
    function queueRenderPage(num) {
    if (pageRendering) {
        pageNumPending = num;
    } else {
        renderPage(num);
    }
    }

    /**
     * Displays previous page.
     */
    function onPrevPage() {
    if (pageNum <= 1) {
        return;
    }
    pageNum--;
    document.getElementById('page_num').textContent = pageNum;
    queueRenderPage(pageNum);
    }
    document.getElementById('prev').addEventListener('click', onPrevPage);

    /**
     * Displays next page.
     */
    function onNextPage() {
    if (pageNum >= pdfDoc.numPages) {
        return;
    }
    pageNum++;
    document.getElementById('page_num').textContent = pageNum;
    queueRenderPage(pageNum);
    }
    document.getElementById('next').addEventListener('click', onNextPage);

    /**
     * Asynchronously downloads PDF.
     */
    let loadingTask = PDFJS.getDocument(url);

    loadingTask.onProgress = function(progress) {
        var percent = parseInt(progress.loaded / progress.total * 100);
        $('#progress').css('width', (pdfPageWidth / 100) * percent + 'px');
    }

    loadingTask.promise.then(function(pdfDoc_) {
        pdfDoc = pdfDoc_;
        document.getElementById('page_count').textContent = pdfDoc.numPages;
        document.getElementById('page_num').textContent = pageNum;

        // Initial/first page rendering
        renderPage(pageNum);
    });

    // PDFJS.getDocument(url).then(function(pdfDoc_) {

    //   pdfDoc = pdfDoc_;
    //   document.getElementById('page_count').textContent = pdfDoc.numPages;

    //   // Initial/first page rendering
    //   renderPage(pageNum);
    // });
};

////////////////////////////////////////////////////////////////////////

// var loadingTask = PDFJS.getDocument(url);
// loadingTask.promise.then(function(pdf) {
//   console.log('PDF loaded');
  
//   // Fetch the first page
//   var pageNumber = 1;
//   pdf.getPage(pageNumber).then(function(page) {
//     console.log('Page loaded');
    
//     var scale = 0.8;
//     var viewport = page.getViewport(scale);

//     // Prepare canvas using PDF page dimensions
//     var canvas = document.getElementById('the-canvas');
//     var context = canvas.getContext('2d');
//     canvas.height = viewport.height;
//     canvas.width = viewport.width;

//     // Render PDF page into canvas context
//     var renderContext = {
//       canvasContext: context,
//       viewport: viewport
//     };
//     var renderTask = page.render(renderContext);
//     renderTask.then(function () {
//       console.log('Page rendered');
//     });
//   });
// }, function (reason) {
//   // PDF loading error
//   console.error(reason);
// });