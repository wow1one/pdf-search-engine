<?php

namespace Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

Class EntityManagerService
{
    private static $entityManager = null;

    public static function create()
    {
        if (is_null(EntityManagerService::$entityManager)) {
            $path = array(__DIR__ . "/../Models/");
            $isDevelopmentMode = true;
            $connection = array(
                'dbname' => DB_NAME,
                'user' => DB_USER,
                'password' => DB_PASSWORD,
                'host' => DB_HOST,
                'driver' => 'pdo_mysql',
                'charset' => 'utf8',
                'driverOptions' => array(
                    1002 => 'SET NAMES utf8'
                )
            );
            $config = Setup::createAnnotationMetadataConfiguration($path, $isDevelopmentMode, null, null, false);
            $entityManager = EntityManager::create($connection, $config);
            EntityManagerService::$entityManager = $entityManager;
        }
        return EntityManagerService::$entityManager;
    }
}
