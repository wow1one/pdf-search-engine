<?php
namespace Models;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PdfFile
 *
 * @ORM\Table(name="pdf_file", uniqueConstraints={@ORM\UniqueConstraint(name="name",columns={"name"})})
 * @ORM\Entity
 */
class PdfFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    // /**
    //  * @var string
    //  *
    //  * @ORM\Column(name="path", type="string", length=255, nullable=false)
    //  */
    // private $path;

    // /**
    //  * @var string
    //  *
    //  * @ORM\Column(name="content", type="text", nullable=false)
    //  */
    // private $content;

    /**
     * One PdfFile has Many Pages.
     * @ORM\OneToMany(targetEntity="PdfPage", mappedBy="pdfFile", cascade={"persist"})
     */
    private $pages;

    public function __construct()
    {
        $this->pages = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getPages()
    {
        return $this->pages;
    }

    public function setPages($pages)
    {
        $this->pages = $pages;
    }

    public function addPage($page)
    {
        $this->pages->add($page);
    }

    // public function getPath()
    // {
    //     return $this->path;
    // }

    // public function setPath($path)
    // {
    //     $this->path = $path;
    // }

    // public function getContent()
    // {
    //     return $this->content;
    // }

    // public function setContent($content)
    // {
    //     $this->content = $content;
    // }
}