<?php
namespace Models;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PdfPage
 *
 * @ORM\Table(name="pdf_page")
 * @ORM\Entity
 */
class PdfPage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Many Pages have One PdfFile.
     * @ORM\ManyToOne(targetEntity="PdfFile", inversedBy="pages")
     * @ORM\JoinColumn(name="pdf_file_id", referencedColumnName="id")
     */
    private $pdfFile;

    /**
     * @var integer
     *
     * @ORM\Column(name="page_num", type="integer", nullable=false)
     */
    private $pageNum;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $content;

    
    public function __construct()
    {
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPdfFile()
    {
        return $this->pdfFile;
    }

    public function setPdfFile($pdfFile)
    {
        $this->pdfFile = $pdfFile;
    }

    public function getPageNum()
    {
        return $this->pageNum;
    }

    public function setPageNum($pageNum)
    {
        $this->pageNum = $pageNum;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }
}