<?php
// <div>
// <button id='prev'>Previous</button>
// <button id='next'>Next</button>
// &nbsp; &nbsp;
// <span>Page: <span id='page_num'></span> / <span id='page_count'></span></span>
// </div>

// <div id='pageContainer' class='pdfViewer singlePageView'></div>

// <canvas id='the-canvas'></canvas>

echo "

<!DOCTYPE html>
<html>
<head>
<link type='text/css' href='./build/text_layer_builder.css' rel='stylesheet'>
<script src='./build/jquery-3.2.1.min.js'></script>
<script src='./build/pdf.js'></script>
<script src='./build/text_layer_builder.js'></script>
</head>
<body>
<h1 id='file-name'>PDF.js example</h1>
<div>
<button id='prev'>Previous</button>
<button id='next'>Next</button>
&nbsp; &nbsp;
<span>Page: <span id='page_num'></span> / <span id='page_count'></span></span>
</div>

<p id='progress' style='height: 2px; width: 0; background-color: #000000' />
<br/>
<div id='container'></div>

</body>
</html>

<script src='./src/js/show-pdf.js'></script>
";