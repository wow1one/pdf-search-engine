<?php

require_once __DIR__ . '/config/config.php';
require_once __DIR__ . '/src/Services/EntityManagerService.php';
require_once __DIR__ . '/src/Models/PdfFile.php';
require_once __DIR__ . '/src/Models/PdfPage.php';